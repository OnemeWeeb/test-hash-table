export const HashTable = (size) => {
	const _store = [];
	const _size = size;

	const hash = (string) => { // Hashing a key
		let index = 0;
		for (let i = 0; i < string.length; i++) {
			index += string.charCodeAt(i) * i + 1;
		}

		return index % _size;
	};

	const findMatchIndex = (list, key) => { // Search index of match between keys
		for (let i = 0; i < list.length; i++) {
			if (list[i][0] === key) return i;
		}
	};

	return {
		dump() { // Hash table output to console
			console.dir(_store);
		},
		setElement(key, element) { // Adding an item to a hash table
			if (!_store[hash(key)]) {
				_store[hash(key)] = [
					[key, element],
				];
			} else {
				const list = _store[hash(key)];
				const index = findMatchIndex(list, key);

				if (index) {
					list[index] = [key, element];
					return list[index];
				}

				list.push([key, element]);
			}
		},
		getElement(key) { // Getting a hash table element by key
			if (_store[hash(key)]) {
				const list = _store[hash(key)];
				const index = findMatchIndex(list, key);

				return (index || index === 0) ? list[index][1] : false;
			}
		},
	};
};

export const pages = [
	'https://www.site.ru/products',
	'https://www.site.ru/main',
	'https://www.site.ru/news',
	'https://www.site1.ru/registration',
	'https://www.site1.ru/search',
	'https://www.site1.ru/media',
];

export const htmlCodeOfPagesYesterday = [
	`<!DOCTYPE html>
	<head>
		<title>Page 1</title>
	</head>
	<body>
		Page 1
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Page 2</title>
	</head>
	<body>
		Page 2
	</body>
	</html>`,
	undefined,
	`<!DOCTYPE html>
	<head>
		<title>Page 4</title>
	</head>
	<body>
		Page 4
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Page 5</title>
	</head>
	<body>
		Page 5
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Page 6</title>
	</head>
	<body>
		Page 6
	</body>
	</html>`,
];

export const htmlCodeOfPagesToday = [
	`<!DOCTYPE html>
	<head>
		<title>Modified page 1</title>
	</head>
	<body>
		qwe
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Page 2</title>
	</head>
	<body>
		Page 2
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Modified page 3</title>
	</head>
	<body>
		zxc
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Page 4</title>
	</head>
	<body>
		Page 4
	</body>
	</html>`,
	`<!DOCTYPE html>
	<head>
		<title>Page 5</title>
	</head>
	<body>
		Page 5
	</body>
	</html>`,
	undefined,
];
