import React from 'react';

const Letter = ({ name, patronymic, pages, previousState, currentState }) => (
	<font face="Helvetica">
		<div>
			<div>Здравствуйте, дорогая {name} {patronymic}</div>
			<div>За последние сутки во вверенных Вам сайтах произошли следующие изменения:</div>
			<p />
			<div>
				<div>{'Исчезли следующие страницы: '}
					<div>{pages.map(key =>
						(currentState.getElement(key) === undefined && previousState.getElement(key) !== undefined) ? <div><a href={key}>{key}</a></div> : '')}
					</div>
				</div>
				<p />
				<div>{'Появились следующие новые страницы: '}
					<div>{pages.map(key =>
						(currentState.getElement(key) !== undefined && previousState.getElement(key) === undefined) ? <div><a href={key}>{key}</a></div> : '')}
					</div>
				</div>
				<p />
				<div>{'Изменились следующие страницы: '}
					<div>{pages.map(key =>
						(currentState.getElement(key) !== undefined && previousState.getElement(key) !== undefined && currentState.getElement(key) !== previousState.getElement(key)) ? <div><a href={key}>{key}</a></div> : '')}
					</div>
				</div>
			</div>
			<p />
			<div>С уважением,</div>
			<div>автоматизированная система мониторинга.</div>
		</div>
	</font>
);


export default Letter;
