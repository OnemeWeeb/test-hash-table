import React, { Component } from 'react';
import Letter from '../Letter/Letter';
import Input from '../Input/Input';
import { HashTable, pages, htmlCodeOfPagesYesterday, htmlCodeOfPagesToday } from '../../common/constants';
import styles from './App.css';

class App extends Component {
	state = {
		isShowing: false,
		isEmpty: false,
		name: '',
		patronymic: '',
		status: '',
	};

	previousState = new HashTable(pages.length); // Hash table of pages status yesterday

	currentState = new HashTable(pages.length); // Hash table of pages status today

	componentDidMount() { // Filling the hash table when initializing the application
		pages.forEach((url, index) =>
			this.previousState.setElement(url, htmlCodeOfPagesYesterday[index]));

		pages.forEach((url, index) =>
			this.currentState.setElement(url, htmlCodeOfPagesToday[index]));

		this.previousState.dump();
		this.currentState.dump();
	}

	handleChange = ({ name, value }) => { // Event handler
		this.setState({
			...this.state,
			isEmpty: false,
			isShowing: false,
			[name]: value,
		});
	}

	onSubmit = (value) => {
		if (value && (this.state.name === '' || this.state.patronymic === '')) {
			this.handleChange({ name: 'isEmpty', value: true });
		} else {
			this.handleChange({ name: 'isShowing', value });
		}
	};

	render() {
		return (
			<div className={styles.app}>
				<form className={styles.body} noValidate>
					<Input
						name='name'
						type='text'
						placeholder='Имя'
						autofocus={true}
						value={this.state.name}
						size={true}
						isEmpty={this.state.isEmpty && (this.state.name === '' || this.state.name.startsWith(' '))}
						changeInputValue={(e) => this.handleChange(e.target)} />
					<Input
						name='patronymic'
						type='text'
						placeholder='Отчество'
						value={this.state.patronymic}
						size={true}
						isEmpty={this.state.isEmpty && (this.state.patronymic === '' || this.state.patronymic.startsWith(' '))}
						changeInputValue={(e) => this.handleChange(e.target)} />
					<div className={styles.submit} onClick={() => this.onSubmit(!this.state.isShowing)}>
						{this.state.isShowing ? 'Спрятать сообщение' : 'Вывести сообщение'}
					</div>
				</form>
				{this.state.isEmpty && <div className={styles.empty}>Не все поля заполнены!</div>}
				{this.state.isShowing
					&& <Letter
						name={this.state.name}
						pages={pages}
						patronymic={this.state.patronymic}
						previousState={this.previousState}
						currentState={this.currentState}
					/>}
			</div>
		);
	}
}

export default App;
