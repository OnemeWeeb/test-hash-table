import React, { useState } from 'react';
import styles from './Input.css';

const Checkout = ({
	name,
	type,
	placeholder,
	isEmpty,
	value,
	autofocus,
	changeInputValue,
}) => {
	const initialState = {
		isFocused: false,
		value: '',
	};
	const [state, setState] = useState(initialState);
	const handleClick = (status) => {
		setState({
			...state,
			isFocused: status,
		});
	};

	return (
		<div className={styles.block} onClick={() => handleClick(true)}
			onBlur={() => handleClick(false)}>
			<label id={`${name}-label`} htmlFor={name} data-shrink='false' className={`${styles.label}
			${(state.isFocused) ? styles.label_focused : ''} 
			${(state.isFocused || value) ? styles.label_shrink : ''}
			${(value === '' || value.startsWith(' ')) ? (isEmpty ? styles.empty_label : '') : styles.filled_label}`}>
				{`${placeholder} *`}
			</label>
			<div className={`${styles.form} ${(value === '' || value.startsWith(' ')) ? (isEmpty ? styles.empty_form : '') : styles.filled_form} ${(state.isFocused) ? styles.form_focused : ''}`}>
				<input
					className={styles.input}
					id={name}
					name={name}
					type={type}
					aria-invalid='false'
					autoComplete='off'
					required
					autoFocus={autofocus}
					value={value}
					onChange={(e) => changeInputValue(e)} />
				<fieldset className={`${styles.fieldset} ${(value === '' || value.startsWith(' ')) ? (isEmpty ? styles.empty : '') : styles.filled}`} aria-hidden='true'>
					<legend className={`${styles.legend} ${(state.isFocused || value) ? styles.legend_active : ''}`}>
						<span className={styles.placeholder}>{`${placeholder} *`}</span>
					</legend>
				</fieldset>
			</div>
		</div>
	);
};

export default Checkout;
